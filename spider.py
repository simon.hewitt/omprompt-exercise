"""Spider.

Usage:
    spider.py <url> <search-text> [--verbose] [--max-depth=<n>]

Options:
  -h --help     Show this screen.
  -v --version  Show version.
  --verbose     Show all pages visited
  --max-depth=<n>

"""
"""
Author: Simon Hewitt
simon.d.hewitt@gmail.com
07799381001
"""

import re
import sys
from http.client import RemoteDisconnected
from typing import List

import requests
from bs4 import BeautifulSoup, Comment
from docopt import docopt
from requests.exceptions import MissingSchema


class Spider:
    def __init__(self, base_url: str, target_text:str, max_depth:int, verbose:bool):
        self.max_depth = int(max_depth) if max_depth else 999 # May be None
        self.verbose = verbose
        self.target_text = target_text
        self.base_url = self._extend_url(base_url)
        self.pages_visited = []
        self.match_count = 0


    def _extend_url(self, url:str) -> str:
        """
        Adds 'www' to URL if necessary
        :param url: URL passed in as parameter
        :return: Same URL but with 'www' added if needed
        """
        url = url.lower()
        if not url.startswith('http'):
            url = 'https://' + url

        pattern = re.compile(r'(^https?://)(.*)')
        match = pattern.match(url.lower())
        if not match:
            print (f"{url} must be a fully qualified domain name including the protocol, such as 'https://bbc.co.uk'")
            sys.exit(4)

        if match.group(2).startswith('www'):
            # Yes there is a www like 'https://www.bbc.co.uk
            return url

        return match.group(1) + 'www.' + match.group(2)

    def _get_clean_links(self, raw_link_array:list) -> List[str]:
        """
        Extract the URL from the tag, remove '#' , and return list of unique URLs
        :param raw_link_array: List of HTML <a> tags
        :return: list of unique URLs
        """
        clean_urls = [r['href'] for r in raw_link_array if not r['href'].startswith('#') ]
        return list(set(clean_urls))

    def _get_web_page(self, fatal:bool) -> str:
        """
        Fetches the raw HTML from the URL. If it fails, exit!
        :fatal: Exits if request fails
        :return: HTML body
        """
        try:
            result = requests.get(self.base_url)
        except (ConnectionError, MissingSchema)  as e:
            print (f"Cannot get from url: {self.base_url}, error:\n{e}")
            sys.exit(1)  # usually fatal, so exit
        except RemoteDisconnected as e:
            print (f"URL {self.base_url} would not reply (RemoteDisconnected), skipping")
            # Generally just a 'control' URL somewhere
        # Other errors - just fail

        if not (result.ok and result.status_code==200):
            print (f"Could not fetch from {self.base_url}, status code: {result.status_code}")
            if fatal:
                sys.exit(2)
        return result.text

    def _tag_visible(self, element):
        if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]'] or \
            isinstance(element, Comment):
            return False
        return True

    def _visit_page(self, target_url: str, depth:int) -> None:
        """
        Visits the target_url page. Searches for target text .
        Prints the URL and the number of matches.
        Finds all usable links on the page, and recursively calls _visit_page on them
        :param target_url: URL to be searched
        :param depth: current recursion depth
        :return: None. Prints output as it goes
        """

        if depth > self.max_depth:
            return

        html_text = self._get_web_page(fatal = (depth == 0)) # Exit if cannot read the 1st URL
        self.pages_visited.append(target_url)

        soup = BeautifulSoup(html_text, 'html.parser')
        if soup and soup.body:
            # Got a web page  body. get the text, that is 'valid' - not <style>, <meta> etc
            # Then find matching text
            text_on_this_page = soup.findAll(text=True)
            visible_text = [str(t.string) for t in text_on_this_page if self._tag_visible(t) and len(t.string) > 1]
                    #  len>1 - remove a lot of '\n' characters etc
            matching_text = [t for t in visible_text if self.target_text in t]
            if matching_text:
                self.match_count += 1
                message = f"URL: {target_url}, {len(matching_text)} references to text: {self.target_text}"
                if self.verbose:
                    message += f"\n\t\t (1st matching line: {matching_text[0]}"
                print (message)
            elif self.verbose:
                print (f"URL: {target_url}, NO reference to text: {self.target_text}")

            # Now recurse all the links on this page
            clean_links = self._get_clean_links(soup.find_all('a', href=True))
            for link in  [l for l in clean_links if self.base_url in l]:
                if link not in self.pages_visited:
                    self._visit_page(target_url=link,  depth=depth+1)
        else:
            print (f"URL {target_url} could not be parsed or had NO text")

    def visit_pages(self):
        self._visit_page(target_url=self.base_url, depth=0)
        return self.match_count

if __name__ == "__main__":
    arguments = docopt(__doc__, version="Spider V1.0")
    base_url = arguments['<url>']
    search_text = arguments['<search-text>']
    verbose = arguments['--verbose']
    print(f"Spider scanning {base_url} for text {search_text}")

    spider = Spider(base_url=base_url,
                    target_text=search_text,
                    max_depth=arguments['--max-depth'],
                    verbose=verbose)
    match_count = spider.visit_pages()
    if verbose:
        print (f"\nAll done, found {match_count} matches to {search_text}")


