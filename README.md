# Spider.py

A program to crawl a URL and return links and text that match a specified text string.

Example:

        python spider.py bbc.co.uk renault
        
Prepare by:

* Create a new directory for the utility, eg `$ mkdir spider; cd spider`
* Set up a new virtual environment based on Python 3.8 
* Download, clone or unzip the code into the new directory
* Run `$ pip install -r requirements.txt`
* Run the utility as given in the command-line options below:

	```
	Spider.
	
	Usage:
	    spider.py <url> <search-text> [--verbose] [--max-depth=<n>]
	
	Options:
	  -h --help     Show this screen.
	  -v --version  Show version.
	  --verbose     Show all pages visited
	  --max-depth=<n>
	
	```

Example: `$ python spider.py bbc.co.uk Renault --verbose --max-depth=4`

Note if `--verbose` is omitted, the program may run silently for some time.

## Design Notes

The program uses 3 external Python libraries:

1. docopt `http://docopt.org/`, a utility to manage command line options. In my view, this is much superior to the standard Python argparse, as it is: Much easier to use; Much clearer in it's intent; Self documenting; Much more brief.   It works by taking a docstring as a definition of the command lines to be parsed, and by convention these are created as the main source file docstring, in triple-quotes at the head of the program, in a form that is very useable as program documentation (and included verbatime above)
2. requests, a staple of HTTP communication. Widely used, robust, straightforward and well suported. It has a tendency to raise, so should generally be in a try/except block.
3. BeautifulSoup. This is an HTML parser, offering capabilities a bit like JQuery. I am not using its full capabilities here, but it makes short work of finding text and links.

The program design is a straightforward recursive call on the main page parser, with an optional depth limit to limit processing time. It is implemented as a class as there are a number of values used that are usefully kept out of the main code base namespace.

### Issues:

The problem turned out to be much harder that I anticipated.  A number of problems cropped up:

Not all links are classic \<a\> tags. In testing on the BBC (bbc.co.uk) web site, a number of links that could be clicked to see detailed stories, turned out on inspection not to be \<a href="address"\> links, but just text, usually in a \<div\> or \<p\> tag. There were no obvious `onclick` attributes in the elements, so I expect there will be JQuery (or similar) code somewehere with a `$('\#element-id').onclick(() => {actions...})` loaded on DOM loading. This makes it much harder to parse.

Some text seems to appear on every page visited although it is not visible on the rendered page. This is common for modern SPA type apps, where all possible content is downloaded but only made visible under browser JavaScript control, usually by making \<div> elements visible or not (using `'display:none' or 'display:block'` or other techniques). The BeautifulSoup parser cannot tell if text is visible or not, so it appears in the report when it may not be actually visible.

Some text is not text, but is in images. There is no solution to this except some form of image processing.

### Alternatives.

My first attempt at this problem was with client-side JavaScript, as JQuery is possibly the best tool for parsing HTML, and provided a much more attractive option for displaying the output. However I neglected to consider CORS (Cross Origin Resource Sharing) limitations, which made it unworkeable in the time available. This can be overcome by delegating the web crawling core to the server side, perhaps using Node  





## Author
Simon Hewitt  
simon.d.hewitt@gmail.com  
07799381001  


        
